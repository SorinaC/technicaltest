**Cucumber Fake News Websites Tests Project**

This project implements 3 tests that intend to prove that the latest news from the Guardian website<br> 
is not fake by searching for said news on Google and asserting the results.<br>

This is not intended as a complete efficient solution to ascertain if a piece of news is fake, its purpose <br>
is to showcase the usage of Cucumber, Java and Selenium.
Feature: The first news displayed on The Guardian's news page is not fake
    """
    The latest news from the Guardian website is taken and is inserted in the Google page search
    field. The results are checked for absolute match, for partial match and the third test ensures
    that no known fake news website are displayed on the first Google results page.

    The fake news websites list below contains the most well known fake news websites. The list can
    be changed and expanded as needed. This list is used for the third test and for excluding
    matching results in the first two tests which come from these fake news websites.
    """
    Background:
        Given I have the following list of known fake news sites:
            | thedcgazette.com |
            | 70news.wordpress.com |
            | abcnews.com.co       |
            | infowars.com         |
            | yournewswire.com     |
        And I am on the News page of The Guardian website
        When I find the newest article on the Guardian's new page

    Scenario: I can find the exact name of the Guardian article on multiple sites via Google
        And I search for the exact article name on Google
        Then I should find at least 1 articles with the same headline

    Scenario: I can find similar article names to the Guardian article on multiple sites via Google
        And I search for the exact article name on Google
        Then I should find at least 2 articles with a 10% word match ratio

    Scenario: There are no known fake news websites displayed when I search for the Guardian article
        And I search for the exact article name on Google
        Then there are 0 fake news sites displayed on the first page
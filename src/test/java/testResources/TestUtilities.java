package testResources;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Implements utility Selenium methods
 */
public class TestUtilities extends DriverSetup{

  /**
   * Explicit Selenium wait for element used for special cases where a custom wait time is required
   * @param waitTime - the time to wait until the expected condition is satisfied
   * @param element - the page element it is waiting for
   */
  public void waitForElement(int waitTime, WebElement element){
    WebDriverWait customWait = new WebDriverWait(getDriver(), waitTime);
    customWait.until(ExpectedConditions.visibilityOf(element));
  }

  public void navigateTo(String url){
    getDriver().get(url);
  }
}
// Copyright 2019 Signal Digital, Inc. All rights reserved.
package testResources;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

/**
 * Initializes the WebDriver and is extended by the FakeNewsTest definition and TestUtilities classes
 */
public class DriverSetup {

  private static WebDriver driver;

  //Initializing the driver
  protected void setUp(){
    WebDriverManager.chromedriver().setup();
    driver = new ChromeDriver();
    driver.manage().window().maximize();
  }

  protected WebDriver getDriver(){
    return driver;
  }
}
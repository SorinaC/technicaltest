// Copyright 2019 Signal Digital, Inc. All rights reserved.
package testResources;

import io.cucumber.java.After;
import io.cucumber.java.Before;

/**
 * Calls the setUp and tearDown methods from the DriverSetup class, as classes that contain Cucumber
 * hooks cannot be extended by other classes
 */
public class CucumberHooks {

  private DriverSetup setup;

  @Before
  public void newSetup(){
    setup = new DriverSetup();
    setup.setUp();
  }

  @After
  public void tearDown(){
    setup.getDriver().quit();
  }
}
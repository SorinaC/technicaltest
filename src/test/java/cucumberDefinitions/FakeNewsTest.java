package cucumberDefinitions;

import java.util.List;

import org.junit.Assert;

import testResources.DriverSetup;
import testResources.TestUtilities;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.GoogleSearchPage;
import pages.GuardianNewsPage;

import static constants.GlobalUrls.GOOGLE_WEBSITE;
import static constants.GlobalUrls.THE_GUARDIAN_WEBSITE;

/**
 * Implements the methods for the Cucumber feature file tests
 */
public class FakeNewsTest extends DriverSetup {

  private TestUtilities testUtilities = new TestUtilities();
  private GuardianNewsPage guardianNewPage;
  private GoogleSearchPage googleSearchPage;
  private String newsHeadline;
  private List<String> fakeNewsSitesList;

  @Given("I have the following list of known fake news sites:")
  public void iHaveTheFollowingListOfKnownFakeNewsSites(DataTable fakeNewsSites) {
    fakeNewsSitesList = fakeNewsSites.asList();
  }

  @And("I am on the News page of The Guardian website")
  public void iAmOnTheNewsPageOfTheGuardianWebsite() {
    testUtilities.navigateTo(THE_GUARDIAN_WEBSITE);
  }

  @And("I search for the exact article name on Google")
  public void iSearchForTheExactArticleNameOnGoogle() {
    testUtilities.navigateTo(GOOGLE_WEBSITE);
    googleSearchPage = new GoogleSearchPage(getDriver());
    googleSearchPage.typeIntoSearchInput(newsHeadline);
  }

  @When("I find the newest article on the Guardian's new page")
  public void iFindTheNewestArticleOnTheGuardianSNewPage() {
    guardianNewPage = new GuardianNewsPage(getDriver());
    newsHeadline = guardianNewPage.getFirstHeadlineText();
    System.out.println("The Guardian article was: " + newsHeadline);
  }

  @Then("I should find at least {int} articles with the same headline")
  public void iShouldFindAtLeastThreeArticlesWithTheSameHeadline(int expectedArticles) {
    int numberFound = googleSearchPage.getExactMatchNumber(newsHeadline, fakeNewsSitesList);
    Assert.assertTrue("Only " + numberFound + " exact matches were found.",
        numberFound > expectedArticles);
  }

  @Then("I should find at least {int} articles with a {int}% word match ratio")
  public void iShouldFindAtLeastThreeArticlesWithSimilarHeadlinesOrContent(int expectedArticles,
      int expectedPercentage) {
    int numberFound = googleSearchPage.getPartialMatchNumber(newsHeadline, expectedPercentage,
        fakeNewsSitesList);
    Assert.assertTrue("Only " + numberFound + " partial matches were found.",
        numberFound > expectedArticles);
  }

  @Then("there are {int} fake news sites displayed on the first page")
  public void thereAreNoFakeNewsSitesDisplayedOnTheFirstPage(int expectedSites) {
    int numberOfFakeNewsSites = googleSearchPage.getNumberOfFakeNewsSitesFound(fakeNewsSitesList);
    Assert.assertTrue("The results contained " + numberOfFakeNewsSites + " fake news sites!",
        numberOfFakeNewsSites == expectedSites);
  }
}
package cucumberDefinitions;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

/**
 * Runner class for the Cucumber tests
 */
@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty"}, glue = {"testResources", "cucumberDefinitions"}, features = "src/test/resources/cucumberTests")

public class Runner {
}
package constants;

/**
 * Declares global Strings for the Urls used in all tests
 */
public class GlobalUrls {

  public static final String THE_GUARDIAN_WEBSITE = "https://www.theguardian.com/tone/news";
  public static final String GOOGLE_WEBSITE = "https://www.google.com";
  public static final String THE_GUARDIAN_ROOT = "https://www.theguardian.com";
}
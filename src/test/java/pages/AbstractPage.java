package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import testResources.TestUtilities;

/**
 * Initializes the page elements for the given page and declares the waitForLoading method
 * to be used in all page object classes
 */
public abstract class AbstractPage {

  protected WebDriverWait wait;
  protected WebDriver driver;
  TestUtilities testUtilities;

  /**
   * Initializes the current page and the TestUtilities class, sets the implicit wait (to be used
   * for all web element interactions) to 30 seconds
   * @param driver the driver set in DriverSetup
   */
  public AbstractPage(WebDriver driver){
    testUtilities = new TestUtilities();
    wait = new WebDriverWait(driver, 30);
    this.driver = driver;
    PageFactory.initElements(driver, this);
    isPageLoaded();
  }

  protected abstract void isPageLoaded();
}
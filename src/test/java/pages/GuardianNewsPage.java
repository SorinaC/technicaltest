package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Maps the elements of the Guardian news page and implements the methods using them
 */
public class GuardianNewsPage extends AbstractPage{

  @FindBy(css = ".pillar-link--News")
  private WebElement newsButton;

  @FindBy(xpath = "(//div[@class = 'fc-item__header'])[1]")
  private WebElement firstHeadline;

  public GuardianNewsPage(WebDriver driver){
    super(driver);
  }

  //waits for the most recent article on the Guardian to be displayed and then returns its title as a
  //String
  public String getFirstHeadlineText(){
    testUtilities.waitForElement(5, firstHeadline);
    return firstHeadline.getText();
  }

  //waits for the news button to be displayed before executing any methods
  @Override
  public void isPageLoaded() {
    wait.until(ExpectedConditions.visibilityOf(newsButton));
  }
}
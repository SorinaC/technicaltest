package pages;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static constants.GlobalUrls.THE_GUARDIAN_ROOT;

/**
 * Maps the elements of the Google Search page and implements the methods using them
 */
public class GoogleSearchPage extends AbstractPage{

  @FindBy(className =  "gLFyf")
  private WebElement searchInput;

  @FindBy(className = "LC20lb")
  private List<WebElement> searchResultsHeadlines;

  @FindBy(className = "TbwUpd")
  private List<WebElement> searchResultsUrls;

  public GoogleSearchPage (WebDriver driver){
    super(driver);
  }

  /**
   * Enters the given text in the Google search field and presses enter when finished
   * @param input the text to be typed in the Google search field
   */
  public void typeIntoSearchInput(String input){
    searchInput.sendKeys(input);
    searchInput.sendKeys(Keys.ENTER);
  }

  /**
   * Compares The Guardian article title to each result on the first page of the Google search
   * results and returns the number of matches found. It excludes results from The Guardian and fake
   * news websites
   * @param news - the latest news article title taken from The Guardian website
   * @param fakeNewsList - the fake news list provided in the feature file
   * @return the number of matches found
   */
  public int getExactMatchNumber(String news, List<String> fakeNewsList){
    Map<String, String> map = getTitleAndUrlMap();
    int i = 0;
    for(Map.Entry<String, String> entry: map.entrySet()){
      if(entry.getKey().contains(getTrimmedNews(news)) && !isGuardianUrl(
          entry.getValue()) && !isFakeNewsSite(news, fakeNewsList)){
        i++;
      }
    }
    return i;
  }

  /**
   * Removes the 'Live " string if it is present in the Guardian article title to prevent it from
   * being compared to the google results. If the Guardian news article title is longer than 59
   * characters it will trim it to a maximum length of 59 characters, as the title of the Google
   * search results is also trimmed at 59 characters by default
   * @param news - The Guardian news article title
   * @return the trimmed Guardian news article title
   */
  private String getTrimmedNews(String news){
    String finalNews = news;
    if(news.length() > 58){
      finalNews = news.substring(0, 58);
    }
    return finalNews.replace("Live ", "");
  }

  /**
   * Verifies if the provided website URL found on the Google search page is The Guardian website URL
   * @param url - website URL found on the Google search page
   * @return true if the provided URL is the Guardian URL and false if it is any other URL
   */
  private boolean isGuardianUrl(String url){
    return url.contains(THE_GUARDIAN_ROOT);
  }

  /**
   * Checks the URL provided from the Google Search page versus the fake news websites list provided
   * in the feature file
   * @param url provided from the Google Search page
   * @param fakeNewsList the list of fake news website provided in the feature file
   * @return true if the URL provided contains one of the fake news websites provided and false if
   * it does not
   */
  private boolean isFakeNewsSite(String url, List<String> fakeNewsList){
    boolean result = false;
    for(String site : fakeNewsList){
      if(url.contains(site)){
        result = true;
        break;
      }
    }
    return result;
  }

  /**
   * Adds the title and URL of the Google results found in a map that preserves the order of the
   * elements added
   * @return return the map containing the Title and URL of the results found on Google
   */
  private Map getTitleAndUrlMap(){
    Map map = new LinkedHashMap();
    for(int i = 0; i < searchResultsHeadlines.size(); i++){
      map.put(searchResultsHeadlines.get(i).getText(), searchResultsUrls.get(i).getText());
    }
    return map;
  }

  /**
   * Compares The Guardian article title to each result on the first page of Google search
   * results and returns the number of partial matches found based on the percentage provided
   * in the feature file. It excludes results from The Guardian and fake news websites
   * @param news - the latest news article title taken from The Guardian website
   * @param fakeNewsList - the fake news list provided in the feature file
   * @param expectedPercentage - the percentage provided in the feature file. It is used for telling
   * the method what percentage of words from the Guardian article is expected to be found in the
   * results found on the Google search page
   * @return the number of matches found
   */
  public int getPartialMatchNumber(String news, int expectedPercentage, List<String> fakeNewsList){
    Map<String, String> map = getTitleAndUrlMap();
    int searchResultsMatched = 0;
    for(Map.Entry<String, String> entry: map.entrySet()){
      if(getWordsMatched(news, entry) > expectedPercentage && !isGuardianUrl(
          entry.getValue()) && !isFakeNewsSite(news, fakeNewsList)){
        System.out.println("# Matched article: " + entry.getKey());
        searchResultsMatched++;
      }
    }
    return searchResultsMatched;
  }

  /**
   * Compares each word from the Guardian news article with the provided result from the first Google
   * results page. After getting the number of words that matched it converts that number to the
   * percentage of words from the Guardian found in the Google result
   * @param news the news from the Guardian website
   * @param entry the provided Google search result
   * @return returns the words matched percentage as an integer
   */
  private int getWordsMatched(String news, Map.Entry<String, String> entry) {
    String[] keywords = news.split(" ");
    int wordsMatched = 0;
    for (String word : keywords) {
      if (entry.getKey().contains(word)) {
        wordsMatched++;
      }
    }
    return wordsMatched * 100 / keywords.length;
  }

  /**
   * Compares each URL found on the Google results page with the fake websites list provided in the
   * feature file and returns the number of matches
   * @param fakeNewsSites the fake websites list provided in the feature file
   * @return the number of matches
   */
  public int getNumberOfFakeNewsSitesFound(List<String> fakeNewsSites){
    int fakeSitesNumberFound = 0;
    for(WebElement element : searchResultsUrls){
      for(String site : fakeNewsSites){
        if(element.getText().contains(site)){
          fakeSitesNumberFound++;
        }
      }
    }
    return fakeSitesNumberFound;
  }

  //waits for the search input field to be displayed before executing any methods
  @Override
  public void isPageLoaded() {
    wait.until(ExpectedConditions.visibilityOf(searchInput));
  }
}